from typing import Optional
from datetime import datetime
from sqlmodel import Field, SQLModel


class AmpalibeUser(SQLModel, table=True):
    __tablename__: str = "amp_user"
    id: Optional[int] = Field(default=None, primary_key=True)
    user_id: str = Field(max_length=50, unique=True, nullable=False)
    action: Optional[str] = None
    last_use: datetime = Field(default=datetime.now(), nullable=False, index=True)
    lang: Optional[str] = Field(min_length=2, max_length=3)

